using System;
using UnityEngine;
using System.Collections;

public static class Constants
{
	public static Vector3 BUY_BUTTONS_START_POSITION = new Vector3 (0, -250, 0);
	public static Vector3 BUY_HEAVY_GUN_BUTTON_END_POSITION = new Vector3 (145, -250, 0);
	public static Vector3 BUY_LIGHTNING_BUTTON_END_POSITION = new Vector3 (280, -270, 0);
	public static Vector3 BUY_MACHINE_GUN_BUTTON_END_POSITION = new Vector3 (-280, -270, 0);
	public static Vector3 BUY_SAW_BUTTON_END_POSITION = new Vector3 (-145, -250, 0);
	public static Vector3 GUN_BASE_POSITION = new Vector3(-4, -300, 0);
}

