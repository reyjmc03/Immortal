﻿package 
{

    public class GameState extends Object
    {
        public static const GAME:int = 0;
        public static const LEVEL_COMPLETE:int = 1;
        public static const PAUSE:int = 2;
        public static const MENU:int = 3;
        public static const GAME_COMPLETE:int = 4;
        public static const MOVIE_ANIM:int = 5;
        public static const LOADING:int = 6;
        public static const GAME_OVER:int = 7;
        public static const HELP:int = 8;
        public static const ENCICLOPEDIA:int = 9;

        public function GameState()
        {
            return;
        }// end function

    }
}
