﻿package 
{
    import flash.display.*;
    import flash.text.*;

    dynamic public class PanelChallenge2 extends MovieClip
    {
        public var txtHealth:TextField;
        public var btClose:SimpleButton;
        public var txtUpgrades:TextField;
        public var txtTower:TextField;
        public var btPlay:SimpleButton;
        public var txtScores:TextField;
        public var txtEye:TextField;
        public var iconEye:MovieClip;
        public var txtCannons:TextField;
        public var txtMaxCannons:TextField;

        public function PanelChallenge2()
        {
            return;
        }// end function

    }
}
