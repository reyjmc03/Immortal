﻿package steampunk_tower_fla
{
    import flash.display.*;
    import flash.text.*;

    dynamic public class btLevel_48 extends MovieClip
    {
        public var btChallenge:SimpleButton;
        public var challengeStar:MovieClip;
        public var bt:MovieClip;
        public var mcStar1:MovieClip;
        public var mcStar2:MovieClip;
        public var mcStar3:MovieClip;
        public var txtLevel:TextField;

        public function btLevel_48()
        {
            addFrameScript(56, this.frame57);
            return;
        }// end function

        function frame57()
        {
            stop();
            return;
        }// end function

    }
}
