﻿package steampunk_tower_fla
{
    import flash.display.*;

    dynamic public class eye2_128 extends MovieClip
    {
        public var a1:MovieClip;
        public var eyeGlow:EyeFlow;

        public function eye2_128()
        {
            addFrameScript(129, this.frame130);
            return;
        }// end function

        function frame130()
        {
            this.eyeGlow.visible = false;
            return;
        }// end function

    }
}
